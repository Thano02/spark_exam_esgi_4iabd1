from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from pyspark.sql.column import Column, _to_java_column, _to_seq

spark = SparkSession.builder.appName("exo4_scala") \
    .master("local[*]") \
    .config('spark.jars', 'src/resources/exo4/udf.jar') \
    .getOrCreate()

def add_category_name(col):
    sc = spark.sparkContext
    add_category_name_udf = sc._jvm.fr.hymaia.sparkfordev.udf.Exo4.addCategoryNameCol()
    return Column(add_category_name_udf.apply(_to_seq(sc, [col], _to_java_column)))

def main():
    df_sell = spark.read.option("header", "true").csv("src/resources/exo4/sell.csv")
    df_cate_name = df_sell.withColumn("category_name", add_category_name(col("category")))
from pyspark.sql import SparkSession
import pyspark.sql.functions as F
from pyspark.sql.window import Window

spark = SparkSession.builder.appName("exo4_python").master("local[*]").getOrCreate()

def add_column(data_frame):
    category_name = F.when(F.col("category") < 6, "food").otherwise("furniture")
    return data_frame.withColumn("category_name", category_name)

def main():
    df_sell = spark.read.option("header", "true").csv("src/resources/exo4/sell.csv")
    df_cate_name = add_column(df_sell)

    window_spec = Window.partitionBy("date", "category")
    df_price_total = df_cate_name.withColumn("total_price_per_category_per_day", F.sum("price").over(window_spec))
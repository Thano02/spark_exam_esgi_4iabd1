import pyspark.sql.functions as F
from pyspark.sql import SparkSession
from pyspark.sql.types import StringType

spark = SparkSession.builder.appName("exo4_python").master("local[*]").getOrCreate()

def add_column_udf(col_name):
    if int(col_name) < 6:
        return "food"
    else:
        return "furniture"

def main():
    df_sell = spark.read.option("header", "true").csv("src/resources/exo4/sell.csv")
    df_cate_name = df_sell.withColumn("category_name", add_column_udf(F.col("category")))
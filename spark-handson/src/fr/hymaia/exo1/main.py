import pyspark.sql.functions as f
from pyspark.sql import SparkSession


def main():
    print("Hello world!")

    spark = SparkSession.builder.master("local[*]").appName("wordcount").getOrCreate()

    input_file_path = "src/resources/exo1/data.csv"
    df = spark.read.csv(input_file_path, header=True, inferSchema=True)

    result = wordcount(df, "text")
    result.show()
    output_path = "data/exo1/output"
    result.write.partitionBy("count").parquet(output_path, mode="overwrite")

def wordcount(df, col_name):
    return df.withColumn('word', f.explode(f.split(f.col(col_name), ' '))) \
        .groupBy('word') \
        .count()

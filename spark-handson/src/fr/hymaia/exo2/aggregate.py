from pyspark.sql import SparkSession
from pyspark.sql.functions import col, count

def main():
    # Création de la SparkSession
    spark = SparkSession.builder.master("local[*]").appName("aggregate_job").getOrCreate()

    # Lecture du fichier clean
    clean_df = spark.read.parquet("data/exo2/")
    clean_df.show()
    # Calcul de la population par département
    population_by_department_df = calculate_population_by_department(clean_df)

    # Écriture du résultat au format CSV
    write_csv(population_by_department_df, "data/exo2/aggregate.csv")

    population_by_department_df.show()
    # Arrêt de la SparkSession
    spark.stop()

def calculate_population_by_department(df):
    return df.groupBy("department").agg(count("*").alias("nb_people")).orderBy(["nb_people", "department"])

def write_csv(df, output_path):
    df.write.mode("overwrite").csv(output_path)

if __name__ == "__main__":
    main()
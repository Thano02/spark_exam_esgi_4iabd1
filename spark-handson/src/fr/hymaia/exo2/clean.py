import pyspark.sql.functions as f
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, substring, when


def main():
    # Création de la SparkSession
    spark = SparkSession.builder.master("local[*]").appName("clean_job").getOrCreate()

    # Lecture des fichiers
    clients_df = read_csv(spark, "src/resources/exo2/clients_bdd.csv")
    villes_df = read_csv(spark, "src/resources/exo2/city_zipcode.csv")

    # Filtrage des clients majeurs
    filtered_clients_df = filter_major_clients(clients_df)

    # Jointure avec les villes
    result_df = join_with_cities(filtered_clients_df, villes_df)

    # Ajout de la colonne département
    result_df = add_department_column(result_df)

    # Écriture du résultat au format Parquet
    write_parquet(result_df, "data/exo2/")

    result_df.show()
    # Arrêt de la SparkSession
    spark.stop()

def read_csv(spark, file_path):
    return spark.read.csv(file_path, header=True, inferSchema=True)

def filter_major_clients(df):
    return df.filter(col("age") >= 18)

def join_with_cities(clients_df, villes_df):
    return clients_df.join(villes_df, "zip", "left_outer")

def add_department_column(df):
    col_zip = substring(col("zip"),0,2)
    condition_corse = when(col("zip")<=20190,"2A").otherwise("2B")
    df = df.withColumn("department", when(col_zip != 20, col_zip).otherwise(condition_corse))
    return df

def write_parquet(df, output_path):
    df.write.parquet(output_path, mode="overwrite")

if __name__ == "__main__":
    main()
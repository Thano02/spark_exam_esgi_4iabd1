from tests.fr.hymaia.spark_test_case import spark
import unittest
from src.fr.hymaia.exo2.clean import filter_major_clients, join_with_cities, add_department_column
from pyspark.sql import Row

class TestMain(unittest.TestCase):
    def test_filter_major_clients(self):
        input = spark.createDataFrame(
            [
                Row(nom='A',age='35'),
                Row(nom='B',age='20'),
                Row(nom='C',age='9'),
                Row(nom='D',age='3')
            ]
        )
        expected = spark.createDataFrame(
            [
                Row(nom='A',age='35'),
                Row(nom='B',age='20')
            ]
        )

        actual = filter_major_clients(input)

        self.assertCountEqual(actual.collect(), expected.collect())
    
    def test_no_column_age(self):
        input = spark.createDataFrame(
            [
                Row(nom='A')
            ]
        )

        with self.assertRaises(Exception):
            filter_major_clients(input)
        
    def test_join_with_cities(self):
        inputClient = spark.createDataFrame(
            [
                Row(name='A',age='20',zip='36780'),
                Row(name='B',age='21',zip='94000')
            ]
        )
        inputVille = spark.createDataFrame(
            [
                Row(city='villeA',zip='36780'),
                Row(city='villeB',zip='94000')
            ]
        )
        expected = spark.createDataFrame(
            [
                Row(name='A',age='20',zip='36780',city='villeA'),
                Row(name='B',age='21',zip='94000',city='villeB')
            ]
        )

        actual = join_with_cities(inputClient,inputVille)

        self.assertCountEqual(actual.collect(), expected.collect())

    def test_join_with_cities(self):
        inputClient = spark.createDataFrame(
            [
                Row(name='A',age='20'),
                Row(name='B',age='21')
            ]
        )
        inputVille = spark.createDataFrame(
            [
                Row(city='villeA',zip='36780'),
                Row(city='villeB',zip='94000')
            ]
        )

        with self.assertRaises(Exception):
            join_with_cities(inputClient,inputVille)

    def test_add_department_column(self):
        inputDepartement = spark.createDataFrame(
            [
                Row(name='A',age='20',zip='36780',city='villeA'),
                Row(name='B',age='21',zip='94000',city='villeB'),
            ]
        )

        expected = spark.createDataFrame(
            [
                Row(name='A',age='20',zip='36780',city='villeA',departement='36'),
                Row(name='B',age='21',zip='94000',city='villeB',departement='94')
            ]
        )

        actual = add_department_column(inputDepartement)

        self.assertCountEqual(actual.collect(), expected.collect())
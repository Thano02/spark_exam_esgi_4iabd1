from tests.fr.hymaia.spark_test_case import spark
import unittest
from src.fr.hymaia.exo2.aggregate import calculate_population_by_department
from pyspark.sql import Row

class TestMain(unittest.TestCase):
    def test_calculate_population_by_department(self):
        inputDepartement = spark.createDataFrame(
            [
                Row(name='A',age='20',zip='36000',city='villeA',department='36'),
                Row(name='B',age='21',zip='94000',city='villeA',department='94'),
                Row(name='C',age='22',zip='75000',city='villeB',department='75')

            ]
        )

        expected = spark.createDataFrame(
            [
                Row(department='36',nb=1),
                Row(department='94',nb=1),
                Row(department='75',nb=1)            ]
        )

        actual = calculate_population_by_department(inputDepartement)
        actual.show()
        self.assertCountEqual(actual.collect(), expected.collect())

    def test_no_column_departement(self):
        inputDepartement = spark.createDataFrame(
            [
                Row(name='A',age='20',zip='36000',city='villeA'),
                Row(name='B',age='21',zip='94000',city='villeA'),
                Row(name='C',age='22',zip='75000',city='villeB')

            ]
        )

        with self.assertRaises(Exception):
            calculate_population_by_department(inputDepartement)

